
// Create a function which return an array by formula i = p x r x t ÷ 100. 
// Use map, filter functions
// Remove negative numbers from array
// Answer is in the format of an array of integer round up to 2 decimal places. Eg. [1.01, 2.23]
// Create unit test using assertion.
var assert = require('assert');

const test = [
 {
   "p": 73,
   "r": 98,
   "t": 11
 },
 {
   "p": 25,
   "r": 37,
   "t": 77
 },
 {
   "p": 34,
   "r": -44,
   "t": 69
 },
 {
   "p": 86,
   "r": 25,
   "t": 27
 },
 {
   "p": 27,
   "r": -38,
   "t": 11
 },
 {
   "p": 4,
   "r": 11,
   "t": 66
 }
]

function mapAndFilter(testArr)
{
  let mappedArr = testArr.map(x => ((x.p * x.r * x.t)/100).toFixed(2));
  
  let filteredArr = mappedArr.filter(x => x>=0);
  
  return filteredArr;
}

assert.deepEqual(mapAndFilter(test) ,[786.94, 712.25, 580.50, 29.04]);

const test1 = [
  {
    "p": 799.789,
    "r": 98,
    "t": 11.57687
  },
  {
    "p": -25,
    "r": -37.0872,
    "t": 77
  },
  {
    "p": 34.97658,
    "r": -44,
    "t": 69
  },
  {
    "p": 86,
    "r": 25,
    "t": 27
  },
  {
    "p": 27,
    "r": -38,
    "t": 11
  },
  {
    "p": 4,
    "r": 11,
    "t": 66
  }
 ]

assert.deepEqual(mapAndFilter(test1) ,['9073.87','713.93','580.50','29.04']);