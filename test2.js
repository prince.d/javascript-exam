var assert = require("assert");

var arr1 = ['0e089d7ee981bec654acb4a678cef7e6', '7adefed65ed36cb257a5e44bc0ad9919', '64450e7b290f8abcfb070a27d5eaf202'];
var arr2 = [{
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {}, {
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}];
// There is an array (arr1) containing hash value and a second array (arr2) contains array of objects. 
// Sort arr2 in the same order as arr1 using "hashKey". Come up with an optimized solution.

//mannually sorted array of arr2
arr3 = [{
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {}, {
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}];

function sorting(arr1, arr2)
{
   var k = 0;
   for(let i = 0; i < arr1.length; i++)
   {
      for(let j = k+1; j < arr2.length; j++)
      {
         if(arr1[i] == arr2[j].hashKey)
         {
            temp = arr2[k]
            arr2[k] = arr2[j];
            arr2[j] = temp;
            k++;
         }
      }
   }
   return arr2;
}

assert.deepEqual(sorting(arr1,arr2), arr3);

//test 2
var arr4 = [{
   "hashKey" : "64450e7b290f8abcfb070a27d5eaf202",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {}, {
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}];

var arr5 = [{
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}, {
   "hashKey" : "64450e7b290f8abcfb070a27d5eaf202",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {}];

assert.deepEqual(sorting(arr1,arr4), arr5);