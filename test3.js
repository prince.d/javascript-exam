// Create a doubly linked list. 
// Write functions for inserting and iterating in linked list. 
// Use js various structures to implement the same

class Node {
    
    constructor(data, nextNode, prevNode)
    {
        this.data = data;
        this.nextNode = nextNode;
        this.prevNode = prevNode;
    }
}

//inserting only at starting of list
function insertNode(head, value)
{
    node = new Node(value, null, null);
    if(head == null)
    {
        head = node;
    }
    else
    {
        head.prevNode = node;
        node.nextNode = head;
        head = node;
    }
    return head;
}

function iterate(head)
{
    let temp = head;
    while(temp.nextNode != null)
    {
        console.log(temp.data);
    }
}

list1 = insertNode(null , 5);
list1 = insertNode(list1, 4);
list1 = insertNode(list1, 3);
list1 = insertNode(list1, 2);